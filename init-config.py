#!/usr/bin/python3
# Yet Another Init Config : Version 0.01
import os

CONFIG_LOCATION = '/etc/yai/yet_another_config.txt'

def _create_config():
    config_file = open(CONFIG_LOCATION, 'w')
    config_file.write("#Yet Another Init Config File. Version 1.0 \n")
    config_file.write("#DO NOT DELETE THIS CONFIG! Even if the location is changed" + \
            " as the init is designed to ALWAYS look here.\n")
    config_file.write("#For options, look in the README file.\n")
    config_file.write("Config_Location: /etc/yai/\n")
    config_file.write("Process_Manager Location: \n")
    config_file.close()

def _load_first(location='/etc/yai/'):
    config = open(location + 'yet_another_config.txt', 'r')
    configuration = config.read()
    config.close()
    data = [] #Process Manager
    split_configuration = configuration.split("\n")
    split_configuration2 = []
    for item in split_configuration:
        if not item.startswith("#"):
            if "#" not in item:
                split_configuration2.append(item)
            elif "#" in item:
                split_configuration2.append(item.split("#")[0])
    split_configuration = split_configuration2
    del split_configuration2

    if split_configuration[0].split(": ")[1] == location:
        _load_first(split_configuration[0].split(": ")[1])
    else:
        if split_configuration[1].split(": ")[1] == "" or split_configuration[1].split(": ")[1] == " ":
            return "None" #Returns None if there is no process manager defined
        elif split_configuration[1].split(": ")[1] != "" and split_configuration[1].split(": ")[1] !=  " ":
            return split_configuration[1].split(": ")[1]
def config_load():
    try:
        config_file = open(CONFIG_LOCATION, 'r')
        config_file.close()
    except OSError:
        _create_config()
        return "None" 
    except IOError:
        _create_config()
        return "None" 
    except:
        return "None" 
    config_file = open(CONFIG_LOCATION, 'r')
    if "Version 1.0 " in config_file.readline():
        config_file.close()
        data = _load_first()
        return data
    else:
        config_file.close()
        return "None" 

data = config_load()
data = str(data)
os.mkfifo("/tmp/yai.fifo")
config_data = open('/tmp/yai.fifo', 'w')
config_data.write(data)
config_data.close()
