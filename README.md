#Yet Another Init Config? (YAIC)

####What is Yet Another Init Config (YAIC)?
Well it's a simple program/module designed for Yet Another Init.

####Installation
Simply put this somewhere that Yet Another Init can start it. Yet Another init looks, by default, in /etc/yai/config

####Requirements
It requires the following:
- Python 3 (Python 3.4 is recomended)
- Something that uses this program (Mainly Yet Another Init)

##PFAQ (Possibly Frequently Asked Questions)

####Why make it seperate?  
Well, because it was it's own project. I only initially included it because the program depended on it, but as it no longer requires it, and because it acts as it's own program. I felt like it should be split.

####Can I write my own program to replace this?  
Feel free! As long as you know how YAI actually reads from the FIFO, you can write your own replacement.

####Where can I get Yet Another Init?  
At the time it can only be obtained from [here](https://bitbucket.org/Chanku/yet-another-init)
